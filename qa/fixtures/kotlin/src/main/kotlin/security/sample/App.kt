
package security.sample

import security.sample.controller.Login
import security.sample.helper.FileHelper
import security.sample.helper.SystemHelper

class App {
    val login = Login()
    val fileHelper = FileHelper()
    val systemHelper = SystemHelper();
}

fun main(args: Array<String>) {
    val app = App();
    app.login.login("admin", "Password")

    app.fileHelper.createDirectory()
    app.systemHelper.executeCommand("./run.sh")
}
